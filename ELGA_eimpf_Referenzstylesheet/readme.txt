﻿************
e-Health REFERENZ-STYLESHEET e-Impfpass
------------------------
Version: 1.6.0+20220116
************

Nutzungsbedingungen:
--------------------
Das "e-Impfpass Stylesheet" wird von der ELGA GmbH bis auf Widerruf unentgeltlich und nicht-exklusiv sowie zeitlich und örtlich unbegrenzt, jedoch beschränkt auf Verwendungen für die Zwecke der "Clinical Document Architecture" (CDA) zur Verfügung gestellt. Veränderungen für die lokale Verwendung sind zulässig. Derartige Veränderungen (sogenannte bearbeitete Fassungen) dürfen ihrerseits publiziert und Dritten zur Weiterverwendung und Bearbeitung zur Verfügung gestellt werden. 
Bei der Veröffentlichung von bearbeiteten Fassungen ist darauf hinzuweisen, dass diese auf Grundlage des von der ELGA GmbH publizierten "eImpf-Stylesheet" erstellt wurden.
Das Stylesheet „eimpf-stylesheet_v1.0.xsl“ ist lediglich für die elektronische Anzeige der Dokumentation im e-Impfpass vorgesehen. Die Terminberechnungen und Handlungsempfehlungen im e-Impfpass dienen nur zur Unterstützung für die AnwenderInnen. Die Verantwortung zur Verabreichung von Impfstoffen oder der Angabe korrekter Impftermin obliegt ausschließlich den dazu berechtigten medizinischen Fachpersonen (ÄrztInnen, ggf. weitere berechtigten Berufsgruppen)
Die Anwendung sowie die allfällige Bearbeitung des "eImpf-Stylesheet" erfolgt in ausschließlicher Verantwortung der AnwenderInnen. Aus der Veröffentlichung, Verwendung und/oder Bearbeitung können keinerlei Rechtsansprüche gegen die ELGA GmbH erhoben oder abgeleitet werden.

************
Optionen:
---------

Das Verhalten des Referenzstylesheets kann über folgende Optionen gesteuert werden (Bitte in der Datei ein- oder auskommentieren):
- setDocumentDeprecated: Dokument als Storniert markieren
- setAuthUser(String userName): Angemeldete BenutzerIn anzeigen
- attachCDAToPDF: Originales CDA-Dokument einbetten
- hideFooterLogo: Logo in Fußzeile ausblenden
- setFontScaleFactor: Schriftart skalieren
- setBannerText(String text): Bannertext definieren
- disableChronologicalView: Chronologische Ansicht deaktivieren, wodurch die Berechnungen und Empfehlungen für die Impfungen angezeigt werden 

************
Known Issues
************
- Bei der Darstellung in den verschiedenen Browsern, müssen einige Einstellungen beachtet werden, damit das Stylesheet richtig angezeigt wird (siehe Changelog_CDA Visualization)
- Die gruppierte Darstellung der kombinierten Impfungen kann aktuell nur in der chronologischen Ansicht aufgerufen werden.
- Die Darstellung des e-Impfpasses auf mobilen Geräten wurde noch nicht optimal umgesetzt.

************
Change-Log
************
1.6.0+20220116
siehe PDF Dokument "Changelog_CDA_Visualization_2022-01"

1.5.0+20211209
siehe PDF Dokument "Changelog_CDA_Visualization_2021-12"

1.4.2+20210911
siehe PDF Dokument "Changelog_CDA_Visualization_2021-09_1"

1.4.1+20210908
siehe PDF Dokument "Changelog_CDA_Visualization_2021-09"

1.3.2+20210624
siehe PDF Dokument "Changelog_CDA_Visualization_2021-06"

1.2.1+20210514
siehe PDF Dokument "Changelog_CDA_Visualization_2021-05_2"

1.1.1+20210422
siehe PDF Dokument "Changelog_CDA_Visualization_2021-04"

1.0.0+20210329
siehe PDF Dokument "Changelog_CDA_Visualization_2021-03_1"

V1.01.001
siehe PDF Dokument "Changelog_CDA Visaulization_2021-03"

V1.00.007.7
siehe PDF Dokument "Changelog_CDA Visaulization_2021-02"

V1.00.007.6
siehe PDF Dokument "Changelog_CDA Visaulization_2021-01"

V1.00.007.5
siehe PDF Dokument "Changelog_CDA Visaulization_2020-12"

V1.00.007.3
siehe PDF Dokument "Changelog_CDA Visaulization_2020-11_1"

V1.00.007.2
siehe PDF Dokument "Changelog_CDA Visaulization_2020-11"

V1.00.005.2
- Änderungen Impfpass für die Testphase
- Chronologische Ansicht eingefügt