************
ELGA CDA2PDF Konverter
************
Version 1.7.0+20220116
************

Das WAR-File kann auf einem Web Application Server eingespielt und verwendet werden (getestet mit Apache Tomcat ab Version 7.0).
Das CDA2PDFOffline Paket steht auch zur offline-Nutzung zur Verf�gung (CDA2PDFOffline.zip).

Die ELGA GmbH stellt den ELGA CDA2PDF Konverter unentgeltlich zur Verf�gung. Die ELGA GmbH �bernimmt keine Haftung f�r die korrekte Funktion, f�r etwaige M�ngel, Sch�den oder Folgefehler. Aus der Verwendung des vorliegenden Programmes kann keinerlei Rechtsanspruch gegen die ELGA GmbH erhoben und/oder abgeleitet werden.

Hinweise zur Anwendung:
- Um Headerdaten in der Darstellung auszublenden (Die Anzeige beginnt dann direkt beim Ersteller/Empf�nger) muss das Attribut "hideheader" mit dem Wert 1 mit �bergeben werden.
- Standardm��ig wird eingef�gter Text normal, gel�schter Text nicht dargestellt. Es gibt die M�glichkeit, eingef�gten Text unterstrichen und kursiv, gel�schten Text durchgestrichen darzustellen. Dem Servlet muss daf�r das Attribut "showrevisionmarks" mit dem Wert 1 �bergeben werden.
Beispiele:
<content revised="insert">Eingef�gter Text</content>
<content revised="delete">Gel�schterText </content>
- Es kann zwischen einer normalen und einer vollst�ndigen PDF-Variante gew�hlt werden. Die "normale" Version enth�lt Folgendes nicht:
	Bereich ab Allgemeine Daten bis einschlie�lich Aufenthalt
	Zus�tzliche Informationen �ber dieses Dokument
_ Der Status des Dokuments kann gew�hlt werden (aktuelles bzw. storniertes Dokument)
- Der angemeldete Benutzer kann �bergeben werden
- Die Originaldatei kann optional eingebettet werden (PDF-A3)
- Der Konverter generiert eine PDF/A-1a Datei, was spezielle Einschr�nkungen mit sich bringt, u.a. sind aus diesem Grund transparente Grafiken in der Originaldatei nicht erlaubt
- Tabellen mit ungleicher Spaltenanzahl k�nnen nicht ausgegeben werden
- Der Tabellenheader kann bei Tabellen �ber mehrere Seiten am Beginn jeder Seite wiederholt werden (Option repeattableheader)

**
ACHTUNG, f�r das Setzen der folgenden Optionen beachten Sie bitte die untenstehenden Hinweise!
- strict mode: wenn aktiviert, werden ung�ltige Dokumente nicht angezeigt (default: aktiviert)
- showTableInBestGuess: wenn aktiviert, werden ung�ltige Tabellen angezeigt ("in best guess mode") (default: deaktiviert)

ACHTUNG:
F�r das Setzen der Optionen strict mode: disabled und showTableInBestGuess: enabled  ist unbedingt Folgendes zu beachten:
Mit dieser Einstellung werden auch ung�ltige Tabellen, z.B.: mit unterschiedlicher Spaltenanzahl innerhalb einer Tabelle im "best guess" Modus angezeigt.
Der Inhalt kann dadurch unter Umst�nden fehlerhaft interpretiert werden.
Diese Tabellen werden durch das Stylesheet mit einem Warnhinweis versehen angezeigt.
Das Umstellen dieser beiden Optionen ist aus haftungsrechtlichen Gr�nden dringend mit dem Verantwortlichen/Ihrem Vorgesetzten abzustimmen. Bei Fragen wenden Sie sich an cda@elga.gv.at.
**

Die ELGA GmbH weist ausdr�cklich darauf hin, dass die Nutzung des vorliegenden Programmes freiwillig und in ausschlie�licher Verantwortung der Anwender erfolgt.
Das Ausblenden des ELGA-Logos mit dem Parameter hideFooterLogo() ist f�r Befunde, die in ELGA registriert werden, nicht gestattet.


Weitere Informationen unter www.elga.gv.at
Mail an office@elga.gv.at

************
Change-Log
************
1.7.0+20220116
siehe PDF Dokument "Changelog_CDA_Visualization_2022-01"

1.6.3+20211214
siehe PDF Dokument "Changelog_CDA_Visualization_2021-12_1"

1.6.2+20211213
siehe PDF Dokument "Changelog_CDA_Visualization_2021-12"

1.5.2+20210911
siehe PDF Dokument "Changelog_CDA_Visualization_2021-09_1"

1.5.1+20210908
siehe PDF Dokument "Changelog_CDA_Visualization_2021-09"

1.4.1+20210624
siehe PDF Dokument "Changelog_CDA_Visualization_2021-06"

1.3.0+20210514
siehe PDF Dokument "Changelog_CDA_Visualization_2021-05_1"

1.2.0+20210510
siehe PDF Dokument "Changelog_CDA_Visualization_2021-05"

1.1.1+20210422
siehe PDF Dokument "Changelog_CDA_Visualization_2021-04"

1.0.0+20210329
siehe PDF Dokument "Changelog_CDA_Visualization_2021-03_1"

1.10.001
siehe PDF Dokument "Changelog_CDA_Visualization_2021-03"

1.09.007.7
siehe PDF Dokument "Changelog_CDA_Visualization_2021-02"

1.09.007.6
siehe PDF Dokument "Changelog_CDA_Visualization_2021-01"

1.09.007.5
siehe PDF Dokument "Changelog_CDA_Visualization_2020-12"

1.09.007.3
siehe PDF Dokument "Changelog_CDA_Visualization_2020-11_1"

1.09.007.2
siehe PDF Dokument "Changelog_CDA_Visualization_2020-11"

1.09.007
siehe PDF Dokument "Changelog_CDA_Visualization_2020-10"

1.09.002.1
siehe beiliegendes PDF Dokument "Changelog_CDA_Visualization_2020-04"

1.08.004
siehe beiliegendes PDF Dokument "Changelog_CDA_Visualization_2019-11"

1.08.003
siehe beiliegendes PDF Dokument "Changelog_CDA_Visualization_2019-10"

1.08.002.3
siehe beiliegendes PDF Dokument "Changelog_CDA_Visualization_2019-05"

1.07.004.1
- Die Skalierung der Schriftgr��e wurde auf max. 115% begrenzt.
- Die Einstellung des Parameters wird am Ende des Dokuments angezeigt.
- Die Bibliothek Apache FOP wurde auf Version 2.3 aktualisiert.

1.07.003.01
- neue Option repeattableheader: Tabellenheader wird am Beginn einer neuen Seite wiederholt, wenn die Tabelle �ber mehrere Seiten geht

1.07.002.2
- Bei langen Ziffernketten in Tabellen erfolgt jetzt ein automatischer Umbruch. Dieser wird, im Unterschied zum Umbruch in Buchstabenketten, ohne Trennzeichen (-)
durchgef�hrt.

1.07.002.1
- Die hinterlegten Value Sets im Stylesheet werden bei jedem Deploy des Stylesheets aktualisiert.
- Die Bildunterschriften werden nun angezeigt.
- Die Anzeige der Uhrzeit (bei vertikaler PDF-Erzeugungsinformation am linken Rand) wurde auf das deutsche Datumsformat umgestellt.
- Die getroffenen Einstellungen/Parameter werden nun in HTML und PDF angezeigt (Am Ende des Dokuments).

1.07.001
- Die �SetId� wird nun in den Dokumentinformationen angezeigt.
- Der Abstand vor abschlie�enden Bemerkungen wurde verkleinert.

1.06.005
- Ein fehlerhafter Abstand im Patientenblock wurde behoben.

V1.06.004
- Sachwalter umbenennen in "Erwachsenenvertreter"
- Anzeige bei unbekanntem Auftraggeber vereinheitlicht
- Korrektur: Hochgestellte Tabellen wurden im PDF �ber Fu�zeile gedruckt
- Anzeige der Sprachf�higkeit von Patienten erm�glicht
- Optimiert: Anzeige mehrerer Unterzeichner, wenn kein rechtlicher Unterzeichner angegeben wird.

V1.06.003
- Text in Tabellen in PDF wird besser an Zeilenbreite angepasst
- Textumbruch in Tabellen in PDF verbessert
- Weitere Behandler werden jetzt angezeigt
- Tausch des Infobutton-Icon
- Es k�nnen nun Tabellen angezeigt werden, die eine unterschiedliche Spaltenanzahl im Body und Header haben. Siehe Parameter �enableShowTableInBestGuess()�
- Die Warnung �You did not close a PDF Document� wurde behoben

V1.06.002.1
- Die Bibliothek Apache FOP wurde auf Version 2.2 aktualisiert. Die Bibliothek Apache PDFBox wurde auf Version 2.0.5 aktualisiert.
- Tabellen mit Spaltenbreiten gr��er als 100% werden nun besser skaliert.
- Zu langer Text im Kapitel �Abschlie�ende Bemerkungen� wird nicht mehr �ber die Fu�zeile hinaus gedruckt.

V1.06.001
- Neue �berschrift "Weitere Informationen zum Patienten" als Gliederung eingef�gt
- Korrektur: Wenn f�r das Land (state) ein Nullflavor angegeben wurde, wurde ein Beistrich nach der Stadt angezeigt.
- Bei den Kontaktdaten der f�r den Aufenthalt verantwortlichen Person (/n1:ClinicalDocument/n1:componentOf/n1:encompassingEncounter/n1:responsibleParty/n1:assignedEntity/n1:telecom) wurden die Kontaktdaten der Organisation (n1:ClinicalDocument/n1:componentOf/n1:encompassingEncounter/n1:location/n1:healthCareFacility/n1:serviceProviderOrganization ) angezeigt.
- Korrektur: F�r ein EIS Basic Dokument wurde im CDA2PDF u.a. "Full Support" angegeben.
- Korrektur: Wenn ein Datums-Wert mit einem Nullflavor angegeben wurde, wurde ein "." angezeigt.
- Apache PDFBox Upgrade von 1.8.7 auf 1.8.13
- PDF-Attachments werden nicht mehr als Bilder behandelt.

V1.05.006
- Darstellung mehrerer Authenticator Elemente erm�glicht, als "unterzeichnet von" untereinander
- Fu�noten wiederholten sich bei Tabellen �ber mehrere Seiten und werden nun nur mehr am Ende der Tabelle angezeigt.
- Ein langer Erstellernamen �berdeckt f�hrt nicht mehr zu einer fehlerhaften Darstellung.
- In der vertikalen Leiste steht statt "Erzeugt von ...." nun "PDF erstellt von ..." und die Schriftgr��e wurde dem restlichem Dokument angepasst.
- Bis zu dieser Version orientierte sich die Anzeige des Auftraggeber Elements im Laborbefund an der templateId des Elements 1.3.6.1.4.1.19376.1.3.3.1.6, was nicht funktionierte, wenn der Auftraggeber mit nullFlavor angegeben wurde. Ab dieser Version wird h�ngt die Anzeige des Auftraggeber Elements an der Document-Level templateIdv
- In der vollst�ndigen Druckansicht wurde vor den Allgemeinen Daten (nach der Trennlinie) die �berschrift: "Weitere Informationen zum Patienten" bzw. "Weitere Informationen zur Patientin" eingef�gt.
- �berarbeitung Anzeige der Fachrichtung und Rolle des Autors

V1.05.005
- Korrektur: lange Texte in Risiko-Section ragen �ber Seite hinaus
- Korrektur. PDF Attachments im Querformat werden abgeschnitten
- Anpassung: CDA2PDF Versionsnummer wird in den PDF Metadaten angezei

V1.05.004
- Fehlerrobustheit: Unterschiedliche Spaltenanzahl in einer Tabelle
(2 Modi werden implementiert:
*strict: bei CDA2PDF wird ein entsprechender Fehlercode retourniert und das aufrufende Modul ist f�r die Fehlerbehandlung zust�ndig (das PDF wird nicht erzeugt). Kann aktiviert/deaktiviert werden.
*debug: "Fehlerhafte" Tabellen werden dargestellt, dient nur der Fehleranalyse und ist f�r keinen produktiven Einsatz zu verwenden. Kann aktiviert/deaktiviert werden.)
- BriefLogo wird vergr��ert
- Fu�notentexte kleiner (90%)
- Korrektur - HTML-Links implementieren
- CDA2PDF Errorhandling

V1.05.003.1
- Tabellen-Footer nicht fehlertolerant

V1.05.003
- Fehlerhafte Umschl�sselung der DocumentLevel Template ID / EIS f�r Befund Bildgebende Diagnostik
- Tabellendarstellung: Korrekte Darstellung des Footers
- Tippfehler "Begin der Leistung"
- Medientype Unterst�tzung (v.a. text/xml)
- Familienname des Patienten fettgedruckt
- Tabellendarstellung - COLSPAN (zus�tzliche Attribute)

V1.05.002
- Umsetzung des Alternativtextes f�r Grafiken - Warnung im CDA2PDF-Logfile Alternate Text
- ELGA-Logo in der Fu�zeile (PDF) ausblenden
- Barrierefreiheit: Brieftext-Logo

V1.05.001.1
- Zeilenumbruch in PDF wie Absatz dargestellt

V1.05.001
- Anzeige bei Storniertem Dokument
- Barrierefreiheit: Alternativtext f�r Grafiken
- Oberer Abstand in "Allergien, Unvertr�glichkeiten und Risiken" verkleinert
- Typ des CDA/XML-Attachment im PDF/A-3 wird angegeben
- Darstellung Zeilenumbr�che verbessert

V1.04.012
- Fehlerrobustheit: Transparenz in eingebetteten Dateien

V1.04.011
- Attachments �berdecken Kopf- und Fu�zeile
- Anzeige der Service Events im Labor (Schlagw�rter)
- Fehlerrobustheit: Unterschiedliche Spaltenanzahl in einer Tabelle
- Anzeige der EIS

V1.04.010
- neue Icons

V1.04.009
- Fehler in CDA2PDF-API  (1.04.007.1) behoben
- Verhalten bei mehreren Autoren
- Anzeige von author.assignedAuthor.code und author.functionCode
- Darstellung von Ger�ten/Software als Autor
- IntendedRecipient ("Ergeht an") nur Person ohne Organisation besser darstellen
- neue Icons

V1.04.008
- URI Sonderzeichen Decodierung in Telecom-Elementen
- Rahmen verschm�lern/entfernen
- Farben reduzieren
- neue Icons
- Einr�ckungen und Gr��e der �berschriften

V1.04.007
- Anzeige des Risiko im Pflegesituationsbericht
- URI Sonderzeichen Decodierung in Telecom-Elementen
- "gesetzlicher Vetreter" in Patientenbox in n�chster Zeile angezeigt
- Aufnahme von neuen Dokumententypen und Dokumentenklassen (LOINC)

V1.04.006
- Verbesserung der Darstellung mit Option "hideheader"
- Verbesserung der Darstellung mit Option "isdeprecated"
- Beschriftung "Verantwortliche Person" bei Aufenthalt

V1.04.005
- In Adress-Elementen kann "%20" als Leerzeichen verwendet werden
- Web-Links werden standardm��ig in einem neuen Tab ge�ffnet
- Anpassungen f�r den Pflegesituationsbericht
- Verbesserungen in der Darstellung von Adressen

V1.04.004
- Korrigiert: Anzeige der Uhrzeit bei 0:00 Uhr
- Korrigiert. Kein Beistrich nach Adresse, wenn Bundesland mit NulFlavor angegeben
- Korrigiert: Medientyp image/gif und imgae/png k�nnen angegeben werden

V1.04.002
- Custodian wird als "Verwahrer des Dokuments" angezeigt
- Die Document-Level Templates f�r die Dokumentenklasse und die EIS werden verk�rzt (nicht mehr separat) dargestellt

V1.04.001
- Optionale PDF-A3 Kompatibilit�t, Original-Datei (XML) kann in die PDF-Datei eingebettet werden
- Vervollst�ndigung der PDF/A Metadaten

V1.04.000
- Darstellung von subscript erm�glicht
- Statt "Sachwalter" wird der Begriff " gesetzlicher Vertreter" verwendet
- Tabellen werden allgemein mit valign top dargestellt

V1.03.010
- Anpassung bei der Darstellung des Autors (lange Organisationsnamen)
- bei der Auswahl der Datei zum Konvertieren ist ein Multiselect m�glich

V1.03.009
- Benutzer und Zeitpunt der Erstellund des PDF anzeigen
- Dateien k�nnen auch �ber den Classloader direkt aus dem WAR geladen werden
- angepasste Konfigurationsm�glichkeit des Loggings
- �nderung der Bibliothek, damit keine tempor�ren Dateien angelegt werden und
  das PDF direkt in einen vom Aufrufer �bergebenen OutputStream geschrieben werden kann.

V1.03.008
- M�glichkeit der Kennzeichnung von stornierten Dokumenten (document state)
- Aktualisierung der  �bersetzung der Klassifkation der Angeh�rigen (personalRelationship)
- Benutzer und Zeitpunkt der Erstellung des PDF k�nnen eingettet werden
- Weitere Anpassungen nach Anforderungen des ELGA-Portals

V1.03.007
- Bessere optische Abtrennung ab "allgemeine Daten"
- Zus�tzliche Adresse "TMP" wird als Pflegeadresse angezeigt, �berschrift "Pflegeadresse"
- Wortlaut der Warnung bei "Allergien, Unvertr�glichkeiten und Risiken" an den Titel der entsprechenden Sektion angepasst


V1.03.006
- Informationen zum Anhang werden in einer Fu�zeile dargestellt, das Deckblatt entf�llt dadurch
- Die Box mit den Ersteller und Empf�nger wurde verkleinert
- es kann zwischen einer normalen und einer vollst�ndigen PDF-Variante gew�hlt werden. Die "normale" Version enth�lt Folgendes nicht:
		Bereich ab Allgemeine Daten bis einschlie�lich Aufenthalt
    		Zus�tzliche Informationen �ber dieses Dokument

V1.03.005
- Zus�tzliche Adresse "TMP" wird als Pflegeadresse angezeigt
- Optionsschalter zum Ausblenden des Druck-Icons
- neue Hinweistexte f�r EIS Basic und Structured
- CDA2PDF Tool zur offline Nutzung erstellt

V1.03.004
- Optimierung im Hinblick auf Security Themen
- Metadaten des Anhangs werden angeziegt (z.B.: Seitenanzahl)
- Optionschalter zur Verwendung externer CSS

V1.03.003
- Audio- und Videodateien (video/mpeg, audio/mpeg) werden als Download zur Verf�gung gestellt
- Allergien und Unvertr�glichkeiten werden dargestellt wie Risiken
- Wenn eine Aktivierung von Javascript notwendig ist, wird im Befund ein entsprechender Hinweis angezeigt
- Tabellen werden nun auch im IE mit alternierenden Hintergrundfarben angezeigt

V1.03.002
- Vereinheitlichung der Benennung von Base64 Decoder
- Darstellungsoptimierung bei ausgeblendeten Headerdaten
- Erweiterung f�r e-Medikation
- Behandlung von abweichenden Dokumentenklassen
- Verbesserungen bei der Darstellung mit Internet Explorer
- Dateiname der PDF-Datei in Browsern vereinheitlicht

V1.03.001
- Headerdaten k�nnen ab jetzt ausgeblendet werden (Die Anzeige beginnt dann direkt beim Ersteller/Empf�nger):
- Eingef�gter Text und gel�schter Text kann optional angezeigt werden
- Korrigiert: Lange Texte in der Box f�r den Ansprechpartner werden abgeteilt
- Vermeidung von Hurenkindern und Schusterjungen
- Trennlinie zwischen prim�ren und sekund�ren Sektionen
